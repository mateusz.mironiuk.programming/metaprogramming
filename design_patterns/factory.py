from inspect import signature, _empty
from itertools import combinations
from typing import List


def construct_mapping(*functions):
    mapping = {}
    for func in functions:
        attr_combinations = _get_attr_combinations(
            mandatory=_get_func_attrs(func, mode='mandatory'),
            optional=_get_func_attrs(func, mode='optional')
        )
        for combination in attr_combinations:
            if combination in mapping:
                raise Exception
            mapping[combination] = func
    return mapping


def _get_attr_combinations(mandatory: tuple, optional: tuple) -> List[frozenset]:
    comb = []
    for num in range(len(optional) + 1):
        comb.extend(
            frozenset(mandatory + combination) for combination
            in combinations(optional, num)
        )
    return comb


def _get_func_attrs(func, mode='all'):
    modes = {'all', 'mandatory', 'optional'}

    assert mode in modes, f"Mode must be from {tuple(modes)}"
    condition = {
        'all': lambda default: True,
        'mandatory': lambda default: default is _empty,
        'optional': lambda default: default is not _empty,
    }

    sig = signature(func)
    return tuple(
        name for name, parameter in sig.parameters.items()
        if condition[mode](parameter.default)
    )


def f1(a, b=1, c=1): return print('f1', a, b, c)


def f2(x, y, z=1): return print('f2', x, y, z)


registered_functions = (f1, f2)


class BaseFactory:
    registered_functions = None

    def __init__(self):
        self.mapping = construct_mapping(*self.registered_functions)

    def __call__(self, **kwargs):
        return self.mapping[frozenset(kwargs.keys())](**kwargs)
