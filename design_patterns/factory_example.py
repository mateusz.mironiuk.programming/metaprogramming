from design_patterns import BaseFactory

if __name__ == '__main__':
    def from_db(url, port=5432):
        return {'detail': 'config_from_db'}

    def from_api(address, token):
        return {'detail': 'config_from_api'}

    def from_file(path):
        return {'detail': 'config_from_file'}

    class ConfigFactory(BaseFactory):
        registered_functions = (from_db, from_api, from_file)

    config = ConfigFactory()

    config(url='postgresql://localhost/mydb?user=me&password=secret', port=1234)
    config(address='https://api.service.com', token='12345678')
    config(path='home/config.yml')