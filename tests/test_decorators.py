import pytest

from decorators import cached, typechecked


def test_typecheck():

    @typechecked
    def func(id: int, name: str, ratio: float, raise_errors=True):
        return id, name, ratio

    func(1, 'Matthew', ratio=1.6)
    with pytest.raises(TypeError):
        func(id='1', name=None, ratio=1)


def test_cached():
    @cached
    def fib(n):
        if n <= 2:
            return 1
        return fib(n - 1) + fib(n - 2)
