from __future__ import annotations
import operator


class Queryset(list):
    separator = '__'

    def get(self, **options):
        return self.filter(**options)[0]

    def filter(self, **options) -> Queryset:
        if not options:
            return self
        key, value = options.popitem()
        return Queryset(self._apply(key, value)).filter(**options)

    def _apply(self, key, value):
        name, *operator = key.split(self.separator)
        operator = operator[0] if operator else 'eq'
        return [obj for obj in self if self._condition(obj, name, operator, value)]

    def _condition(self, obj, name, operator, value):
        attr = getattr(obj, name)

        if operator == 'eq':
            return attr == value

        if operator == 'ne':
            return attr != value

        if operator == 'in':
            return attr in value

        if operator == 'lt':
            return attr < value

        if operator == 'lte':
            return attr <= value

        if operator == 'gt':
            return attr > value

        if operator == 'gte':
            return attr >= value

    def first(self):
        try:
            return self[0]
        except IndexError:
            return None

    def order_by(self, key) -> Queryset:
        reverse = False
        if key.startswith('-'):
            key = key[1:]
            reverse = True

        return Queryset(
            sorted(self, key=operator.attrgetter(key), reverse=reverse)
        )


class Base:
    objects = None

    def __init_subclass__(cls, **kwargs):
        cls.objects = Queryset()
        super().__init_subclass__()

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        cls.objects.append(instance)
        return instance


class Customer(Base):
    def __init__(self, id, name, email):
        self.id = id
        self.name = name
        self.email = email

    def __repr__(self):
        return f'{self.__class__.__name__}(id={self.id!r}, name={self.name!r}, email={self.email!r})'


class Product(Base):
    pass


if __name__ == '__main__':
    Customer(id=0, name='Ed', email='ed@gmail.com')
    Customer(id=1, name='Edd', email='edd@hotmail.com')
    Customer(id=2, name='Eddie', email='eddie@hotmail.com')
    Customer(id=3, name='Addie', email='Addie+1@hotmail.com')

    customers = Customer.objects.filter(id__gte=1).order_by('name')
    breakpoint()