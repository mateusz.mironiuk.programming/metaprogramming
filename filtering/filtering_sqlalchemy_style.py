from functools import reduce


class Filterable:
    def __init__(self, name):
        self.name = name

    def __set__(self, instance, value):
        instance.__dict__[self.name] = value

    def __gt__(self, other):
        return self.name, 'gt', other

    def __lt__(self, other):
        return self.name, 'lt', other

    def __eq__(self, other):
        return self.name, 'eq', other


class Service:
    registry = []
    idx = Filterable(name='idx')

    @classmethod
    def filter(cls, *expressions):
        querysets = []
        for expression in expressions:
            attr, operator, value = expression
            queryset = set()
            for obj in cls.registry:
                if operator == 'gt':
                    if getattr(obj, attr) > value:
                        queryset.add(obj)
                if operator == 'lt':
                    if getattr(obj, attr) < value:
                        queryset.add(obj)
                if operator == 'eq':
                    if getattr(obj, attr) == value:
                        queryset.add(obj)

                querysets.append(queryset)
        if not querysets:
            return set()
        elif len(querysets) == 1:
            return querysets[0]
        result = reduce(set.intersection, querysets)
        return result

    def __init__(self, idx):
        self.idx = idx

    def __new__(cls, *args, **kwargs):
        obj = super(Service, cls).__new__(cls)
        cls.registry.append(obj)
        return obj


Service(idx=2)
Service(idx=4)
Service(idx=7)
Service(idx=11)

Service.filter(Service.idx > 3, Service.idx < 10)