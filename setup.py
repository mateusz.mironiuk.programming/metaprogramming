#!/usr/bin/env python

from distutils.core import setup

setup(
    name='python-metaprogramming',
    version='1.0',
    description='Python Metaprogramming Tools',
    author='Mateusz Mironiuk',
    packages=[
        'frozendict==1.2',
        'pytest==4.5.0',
    ],
)
