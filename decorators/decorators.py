from inspect import signature
from functools import wraps
from frozendict import frozendict


def typechecked(func):
    ann = func.__annotations__
    sig = signature(func)

    @wraps(func)
    def wrapper(*args, **kwargs):
        errors = []
        arguments = sig.bind(*args, **kwargs).arguments
        for name, value in arguments.items():
            if not isinstance(value, ann[name]):
                errors.append(
                    f'function {func!r}: argument {name!r}'
                    f' must be {ann[name]!r}, not {type(value)!r}.')
        if errors:
            raise TypeError('\n'.join(errors))
        return func(*args, **kwargs)
    return wrapper


def cached(func):
    cache = {}
    sig = signature(func)

    @wraps(func)
    def wrapper(*args, **kwargs):
        arguments = frozendict(sig.bind(*args, **kwargs).arguments)
        if arguments not in cache:
            cache[arguments] = func(*args, **kwargs)
        return cache[arguments]
    return wrapper
